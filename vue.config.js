const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = {
    configureWebpack: {
        plugins: [new BundleAnalyzerPlugin()]
    },
    publicPath: '/discov-angou'
};