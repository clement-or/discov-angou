import { createApp } from 'vue'
import App from './App.vue'

require('@/assets/main.sass')

createApp(App).mount('#app')
